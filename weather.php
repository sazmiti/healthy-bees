 <?php

error_reporting(E_ALL & ~E_NOTICE);









/*METEO INFORMATION 5 DAY*/



$url_forecast = "http://api.openweathermap.org/data/2.5/forecast?id=3037656&lang=fr&units=metric&APPID=5fbc7c521f72d818d972380a7825e398";



$forecast_contents = file_get_contents($url_forecast);

$forecast_data=json_decode($forecast_contents);



/*VARIABLES*/

$dateoftoday = date("Y-m-d");

$increment = 0;



/*SELECTION DES LIGNES POUR LES 5DAY*/

for ($i = 0; $i <= 39; $i++) {  //2018-02-18 18:00:00

    $date = $forecast_data->list[$i]->dt_txt;

    $midi=strpos($date, "12:00:00");

    

    if($midi !== false)

    {

        $notoday = strpos($date, $dateoftoday);

        

        if($notoday !== false)

        {

            /*IT'S TODAY SO DO NOTHING*/

        }

        else

        {

            /*IT'S NOT TODAY SAVE THE LINE NUMBER AND THE DATE*/

            switch($increment)

            {

                case 0:

                    $numerolistdate1 = $i;

                    $date1 = $date;

                    $increment = 1;

                    break;

                case 1:

                    $numerolistdate2 = $i;

                    $date2 = $date;

                    $increment = 2;

                    break;

                case 2:

                    $numerolistdate3 = $i;

                    $date3 = $date;

                    $increment =3;

                    break;

                case 3:

                    $numerolistdate4 = $i;

                    $date4 = $date;

                    $increment = 4;

                    break;

                case 4:

                    $numerolistdate5 = $i;

                    $date5 = $date;

                    $increment = 5;

                    break;

            }

        }

    }

}



/*

$numerolistdate1 -> DEMAIN

$date1 -> DEMAIN

*/



/*CHOIX DES ICONS*/

$geticon = str_replace("n", "d", $forecast_data->list[0]->weather[0]->icon);

$icon= $geticon . ".png";



$geticon1 = str_replace("n", "d", $forecast_data->list[$numerolistdate1]->weather[0]->icon);

$icon1= $geticon1 . ".png";



$geticon2 = str_replace("n", "d", $forecast_data->list[$numerolistdate2]->weather[0]->icon);

$icon2= $geticon2 . ".png";



$geticon3 = str_replace("n", "d", $forecast_data->list[$numerolistdate3]->weather[0]->icon);

$icon3= $geticon3 . ".png";



$geticon4 = str_replace("n", "d", $forecast_data->list[$numerolistdate4]->weather[0]->icon);

$icon4= $geticon4 . ".png";



/*CHOIX DE LA TEMPERATURE MAX*/

$temp_max1 = max($forecast_data->list[$numerolistdate1-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate1-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate1-2]->main->temp_max,

    $forecast_data->list[$numerolistdate1-1]->main->temp_max,

    $forecast_data->list[$numerolistdate1]->main->temp_max,

    $forecast_data->list[$numerolistdate1+1]->main->temp_max,

    $forecast_data->list[$numerolistdate1+2]->main->temp_max,

    $forecast_data->list[$numerolistdate1+3]->main->temp_max);



$temp_max2 = max($forecast_data->list[$numerolistdate2-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate2-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate2-2]->main->temp_max,

    $forecast_data->list[$numerolistdate2-1]->main->temp_max,

    $forecast_data->list[$numerolistdate2]->main->temp_max,

    $forecast_data->list[$numerolistdate2+1]->main->temp_max,

    $forecast_data->list[$numerolistdate2+2]->main->temp_max,

    $forecast_data->list[$numerolistdate2+3]->main->temp_max);



$temp_max3 = max($forecast_data->list[$numerolistdate3-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate3-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate3-2]->main->temp_max,

    $forecast_data->list[$numerolistdate3-1]->main->temp_max,

    $forecast_data->list[$numerolistdate3]->main->temp_max,

    $forecast_data->list[$numerolistdate3+1]->main->temp_max,

    $forecast_data->list[$numerolistdate3+2]->main->temp_max,

    $forecast_data->list[$numerolistdate3+3]->main->temp_max);



$temp_max4 = max($forecast_data->list[$numerolistdate4-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate4-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate4-2]->main->temp_max,

    $forecast_data->list[$numerolistdate4-1]->main->temp_max,

    $forecast_data->list[$numerolistdate4]->main->temp_max,

    $forecast_data->list[$numerolistdate4+1]->main->temp_max,

    $forecast_data->list[$numerolistdate4+2]->main->temp_max,

    $forecast_data->list[$numerolistdate4+3]->main->temp_max);





/*CHOIX DE LA TEMPERATURE MIN*/

$temp_min1 = min($forecast_data->list[$numerolistdate1-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate1-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate1-2]->main->temp_min,

    $forecast_data->list[$numerolistdate1-1]->main->temp_min,

    $forecast_data->list[$numerolistdate1]->main->temp_min,

    $forecast_data->list[$numerolistdate1+1]->main->temp_min,

    $forecast_data->list[$numerolistdate1+2]->main->temp_min,

    $forecast_data->list[$numerolistdate1+3]->main->temp_min);



$temp_min2 = min($forecast_data->list[$numerolistdate2-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate2-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate2-2]->main->temp_min,

    $forecast_data->list[$numerolistdate2-1]->main->temp_min,

    $forecast_data->list[$numerolistdate2]->main->temp_min,

    $forecast_data->list[$numerolistdate2+1]->main->temp_min,

    $forecast_data->list[$numerolistdate2+2]->main->temp_min,

    $forecast_data->list[$numerolistdate2+3]->main->temp_min);



$temp_min3 = min($forecast_data->list[$numerolistdate3-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate3-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate3-2]->main->temp_min,

    $forecast_data->list[$numerolistdate3-1]->main->temp_min,

    $forecast_data->list[$numerolistdate3]->main->temp_min,

    $forecast_data->list[$numerolistdate3+1]->main->temp_min,

    $forecast_data->list[$numerolistdate3+2]->main->temp_min,

    $forecast_data->list[$numerolistdate3+3]->main->temp_min);



$temp_min4 = min($forecast_data->list[$numerolistdate4-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate4-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate4-2]->main->temp_min,

    $forecast_data->list[$numerolistdate4-1]->main->temp_min,

    $forecast_data->list[$numerolistdate4]->main->temp_min,

    $forecast_data->list[$numerolistdate4+1]->main->temp_min,

    $forecast_data->list[$numerolistdate4+2]->main->temp_min,

    $forecast_data->list[$numerolistdate4+3]->main->temp_min);





/*DATE FOR 5 DAY*/

$deuxjours = strftime("%A",strtotime('+2 day'));
$troisjours = strftime("%A",strtotime('+3 day'));
$quatrejours = strftime("%A",strtotime('+4 day'));

switch ($deuxjours) {
    case 'Monday':
        $deuxjours = "Lundi";
        break;
    case 'Tuesday':
        $deuxjours = "Mardi";
        break;
    case 'Wednesday':
        $deuxjours = "Mercredi";
        break;
    case 'Thursday':
        $deuxjours = "Jeudi";
        break;
    case 'Friday':
        $deuxjours = "Vendredi";
        break;
    case 'Saturday':
        $deuxjours = "Samedi";
        break;
    case 'Sunday':
        $deuxjours = "Dimanche";
        break;
}
switch ($troisjours) {
    case 'Monday':
        $troisjours = "Lundi";
        break;
    case 'Tuesday':
        $troisjours = "Mardi";
        break;
    case 'Wednesday':
        $troisjours = "Mercredi";
        break;
    case 'Thursday':
        $troisjours = "Jeudi";
        break;
    case 'Friday':
        $troisjours = "Vendredi";
        break;
    case 'Saturday':
        $troisjours = "Samedi";
        break;
    case 'Sunday':
        $troisjours = "Dimanche";
        break;
}
switch ($quatrejours) {
    case 'Monday':
        $quatrejours = "Lundi";
        break;
    case 'Tuesday':
        $quatrejours = "Mardi";
        break;
    case 'Wednesday':
        $quatrejours = "Mercredi";
        break;
    case 'Thursday':
        $quatrejours = "Jeudi";
        break;
    case 'Friday':
        $quatrejours = "Vendredi";
        break;
    case 'Saturday':
        $quatrejours = "Samedi";
        break;
    case 'Sunday':
        $quatrejours = "Dimanche";
        break;
}


$deux = strftime("%e",strtotime('+2 day'));
$trois = strftime("%e",strtotime('+3 day'));
$quatre = strftime("%e",strtotime('+4 day'));


$cityname = $clima->name; 



?>
<body>

<section id="barre_meteo" class="row hidden-xs">

	<div class="container col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">


    	    <h1 class="text-center"><?php echo $cityname;?></h1>

    		<div id="box_meteo" class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

    		    <center>

    			<h2>Maintenant</h2>

    			<img class="icon_meteo" src="images/icon_<?php echo $icon?>" alt="icon_meteo.png"><br>
    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $forecast_data->list[0]->main->temp_min?> °C</h3><br>
            
    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $forecast_data->list[0]->main->temp_max?> °C</h3>

    			

    			<h3 class="text_type_meteo"><?php echo $forecast_data->list[0]->weather[0]->description?></h3>

    			</center>

    		</div>

    		

    <!Demain>

    		

    		<div id="box_meteo1" onclick="animation_box_meteo1()" class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">

    		    

    		    <center>

    			<h2>Demain</h2>

    			<img class="icon_meteo" src="images/icon_<?php echo $icon1?>" alt="icon_meteo.png"><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $temp_min1;?> °C</h3><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $temp_max1?> °C</h3>

    			

    			<h3 class="text_type_meteo"><?php echo $forecast_data->list[$numerolistdate1]->weather[0]->description?></h3>

    		</div>

    		

    <!Après demain>

    		

    		<div id="box_meteo2" onclick="animation_box_meteo2()" class="col-xs-6 col-sm-6 col-md-3 col-lg-3">

    		    

    		    <center>

    			<h2><?php echo $deuxjours. " " .$deux;?></h2>

    			<img class="icon_meteo" src="images/icon_<?php echo $icon2?>" alt="icon_meteo.png"><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $temp_min2?> °C</h3><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $temp_max2?> °C</h3>

    			

    			<h3 class="text_type_meteo"><?php echo $forecast_data->list[$numerolistdate2]->weather[0]->description?></h3>

    			</center>

    		</div>


    <!Dans 3 jours>

    		

    		<div id="box_meteo3" onclick="animation_box_meteo3()" class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">

    		    

    		    <center>

    			<h2><?php echo $troisjours. " " .$trois;?></h2>

    			<img class="icon_meteo" src="images/icon_<?php echo $icon3?>" alt="icon_meteo.png"><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $temp_min3?> °C</h3><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $temp_max3?> °C</h3>

    			

    			<h3 class="text_type_meteo"><?php echo $forecast_data->list[$numerolistdate3]->weather[0]->description?></h3>

    			</center>

    		</div>
		


    		

    		</center>

		</div>

	</div>

</section>

</body>






