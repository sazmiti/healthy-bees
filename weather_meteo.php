 <?php

error_reporting(E_ALL & ~E_NOTICE);



/*METEO INFORMATION 5 DAY*/



$url_forecast = "http://api.openweathermap.org/data/2.5/forecast?id=3037656&lang=fr&units=metric&APPID=5fbc7c521f72d818d972380a7825e398";



$forecast_contents = file_get_contents($url_forecast);

$forecast_data=json_decode($forecast_contents);



/*VARIABLES*/

$dateoftoday = date("Y-m-d");

$increment = 0;



/*SELECTION DES LIGNES POUR LES 5DAY*/

for ($i = 0; $i <= 39; $i++) {  //2018-02-18 18:00:00

    $date = $forecast_data->list[$i]->dt_txt;

    $midi=strpos($date, "12:00:00");

    

    if($midi !== false)

    {

        $notoday = strpos($date, $dateoftoday);

        

        if($notoday !== false)

        {

            /*IT'S TODAY SO DO NOTHING*/

        }

        else

        {

            /*IT'S NOT TODAY SAVE THE LINE NUMBER AND THE DATE*/

            switch($increment)

            {

                case 0:

                    $numerolistdate1 = $i;

                    $date1 = $date;

                    $increment = 1;

                    break;

                case 1:

                    $numerolistdate2 = $i;

                    $date2 = $date;

                    $increment = 2;

                    break;

                case 2:

                    $numerolistdate3 = $i;

                    $date3 = $date;

                    $increment =3;

                    break;

                case 3:

                    $numerolistdate4 = $i;

                    $date4 = $date;

                    $increment = 4;

                    break;

                case 4:

                    $numerolistdate5 = $i;

                    $date5 = $date;

                    $increment = 5;

                    break;

            }

        }

    }

}



/*

$numerolistdate1 -> DEMAIN

$date1 -> DEMAIN

*/



/*CHOIX DES ICONS*/

$geticon = str_replace("n", "d", $forecast_data->list[0]->weather[0]->icon);

$icon= $geticon . ".png";



$geticon1 = str_replace("n", "d", $forecast_data->list[$numerolistdate1]->weather[0]->icon);

$icon1= $geticon1 . ".png";



$geticon2 = str_replace("n", "d", $forecast_data->list[$numerolistdate2]->weather[0]->icon);

$icon2= $geticon2 . ".png";



$geticon3 = str_replace("n", "d", $forecast_data->list[$numerolistdate3]->weather[0]->icon);

$icon3= $geticon3 . ".png";



$geticon4 = str_replace("n", "d", $forecast_data->list[$numerolistdate4]->weather[0]->icon);

$icon4= $geticon4 . ".png";



/*CHOIX DE LA TEMPERATURE MAX*/

$temp_max1 = max($forecast_data->list[$numerolistdate1-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate1-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate1-2]->main->temp_max,

    $forecast_data->list[$numerolistdate1-1]->main->temp_max,

    $forecast_data->list[$numerolistdate1]->main->temp_max,

    $forecast_data->list[$numerolistdate1+1]->main->temp_max,

    $forecast_data->list[$numerolistdate1+2]->main->temp_max,

    $forecast_data->list[$numerolistdate1+3]->main->temp_max);



$temp_max2 = max($forecast_data->list[$numerolistdate2-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate2-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate2-2]->main->temp_max,

    $forecast_data->list[$numerolistdate2-1]->main->temp_max,

    $forecast_data->list[$numerolistdate2]->main->temp_max,

    $forecast_data->list[$numerolistdate2+1]->main->temp_max,

    $forecast_data->list[$numerolistdate2+2]->main->temp_max,

    $forecast_data->list[$numerolistdate2+3]->main->temp_max);



$temp_max3 = max($forecast_data->list[$numerolistdate3-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate3-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate3-2]->main->temp_max,

    $forecast_data->list[$numerolistdate3-1]->main->temp_max,

    $forecast_data->list[$numerolistdate3]->main->temp_max,

    $forecast_data->list[$numerolistdate3+1]->main->temp_max,

    $forecast_data->list[$numerolistdate3+2]->main->temp_max,

    $forecast_data->list[$numerolistdate3+3]->main->temp_max);



$temp_max4 = max($forecast_data->list[$numerolistdate4-4]->main->temp_max, 

    $forecast_data->list[$numerolistdate4-3]->main->temp_max, 

    $forecast_data->list[$numerolistdate4-2]->main->temp_max,

    $forecast_data->list[$numerolistdate4-1]->main->temp_max,

    $forecast_data->list[$numerolistdate4]->main->temp_max,

    $forecast_data->list[$numerolistdate4+1]->main->temp_max,

    $forecast_data->list[$numerolistdate4+2]->main->temp_max,

    $forecast_data->list[$numerolistdate4+3]->main->temp_max);





/*CHOIX DE LA TEMPERATURE MIN*/

$temp_min1 = min($forecast_data->list[$numerolistdate1-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate1-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate1-2]->main->temp_min,

    $forecast_data->list[$numerolistdate1-1]->main->temp_min,

    $forecast_data->list[$numerolistdate1]->main->temp_min,

    $forecast_data->list[$numerolistdate1+1]->main->temp_min,

    $forecast_data->list[$numerolistdate1+2]->main->temp_min,

    $forecast_data->list[$numerolistdate1+3]->main->temp_min);



$temp_min2 = min($forecast_data->list[$numerolistdate2-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate2-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate2-2]->main->temp_min,

    $forecast_data->list[$numerolistdate2-1]->main->temp_min,

    $forecast_data->list[$numerolistdate2]->main->temp_min,

    $forecast_data->list[$numerolistdate2+1]->main->temp_min,

    $forecast_data->list[$numerolistdate2+2]->main->temp_min,

    $forecast_data->list[$numerolistdate2+3]->main->temp_min);



$temp_min3 = min($forecast_data->list[$numerolistdate3-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate3-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate3-2]->main->temp_min,

    $forecast_data->list[$numerolistdate3-1]->main->temp_min,

    $forecast_data->list[$numerolistdate3]->main->temp_min,

    $forecast_data->list[$numerolistdate3+1]->main->temp_min,

    $forecast_data->list[$numerolistdate3+2]->main->temp_min,

    $forecast_data->list[$numerolistdate3+3]->main->temp_min);



$temp_min4 = min($forecast_data->list[$numerolistdate4-4]->main->temp_min, 

    $forecast_data->list[$numerolistdate4-3]->main->temp_min, 

    $forecast_data->list[$numerolistdate4-2]->main->temp_min,

    $forecast_data->list[$numerolistdate4-1]->main->temp_min,

    $forecast_data->list[$numerolistdate4]->main->temp_min,

    $forecast_data->list[$numerolistdate4+1]->main->temp_min,

    $forecast_data->list[$numerolistdate4+2]->main->temp_min,

    $forecast_data->list[$numerolistdate4+3]->main->temp_min);





/*DATE FOR 5 DAY*/


$deuxjours = strftime("%A",strtotime('+2 day'));
$troisjours = strftime("%A",strtotime('+3 day'));
$quatrejours = strftime("%A",strtotime('+4 day'));

switch ($deuxjours) {
    case 'Monday':
        $deuxjours = "Lundi";
        break;
    case 'Tuesday':
        $deuxjours = "Mardi";
        break;
    case 'Wednesday':
        $deuxjours = "Mercredi";
        break;
    case 'Thursday':
        $deuxjours = "Jeudi";
        break;
    case 'Friday':
        $deuxjours = "Vendredi";
        break;
    case 'Saturday':
        $deuxjours = "Samedi";
        break;
    case 'Sunday':
        $deuxjours = "Dimanche";
        break;
}
switch ($troisjours) {
    case 'Monday':
        $troisjours = "Lundi";
        break;
    case 'Tuesday':
        $troisjours = "Mardi";
        break;
    case 'Wednesday':
        $troisjours = "Mercredi";
        break;
    case 'Thursday':
        $troisjours = "Jeudi";
        break;
    case 'Friday':
        $troisjours = "Vendredi";
        break;
    case 'Saturday':
        $troisjours = "Samedi";
        break;
    case 'Sunday':
        $troisjours = "Dimanche";
        break;
}
switch ($quatrejours) {
    case 'Monday':
        $quatrejours = "Lundi";
        break;
    case 'Tuesday':
        $quatrejours = "Mardi";
        break;
    case 'Wednesday':
        $quatrejours = "Mercredi";
        break;
    case 'Thursday':
        $quatrejours = "Jeudi";
        break;
    case 'Friday':
        $quatrejours = "Vendredi";
        break;
    case 'Saturday':
        $quatrejours = "Samedi";
        break;
    case 'Sunday':
        $quatrejours = "Dimanche";
        break;
}


$deux = strftime("%e",strtotime('+2 day'));
$trois = strftime("%e",strtotime('+3 day'));
$quatre = strftime("%e",strtotime('+4 day'));



?>



<section id="barre_meteo_principale" class="row">

    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">

	    <center>

    	    <h1><?php echo $cityname;?></h1>

	    

    		<div class="row">
                <div class="col-md-12">

    		        

    			<h2>Maintenant</h2>

    			<img class="icon_meteo" src="images/icon_<?php echo $icon?>" alt="icon_meteo.png"><br>
    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $forecast_data->list[0]->main->temp_min?> °C</h3><br>

    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $forecast_data->list[0]->main->temp_max?> °C</h3>


    			<h3 class="text_type_meteo"><?php echo $forecast_data->list[0]->weather[0]->description?></h3>

    			


    		</div>

    		

    <!Demain>

    		<div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    			<h2>Demain</h2>
    			<div class="meteo_box_byday">

        			<img class="icon_meteo" src="images/icon_<?php echo $icon1?>" alt="icon_meteo.png"><br>

        			

        			<h3 class="tempaff">

        			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

        			<?php echo $temp_min1;?> °C</h3><br>

        			<h3 class="tempaff">

        			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

        			<?php echo $temp_max1?> °C</h3><br>

    			</div>
                </div>
                <div class="row">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    			<?php

    			    for ($i = -4; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate1+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate1+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate1+$i]->dt_txt,-8,5);

                        echo '

                        <div class="hidden-xs hidden-sm col-md-1 col-lg-1 hoursforecast">

                        <h3>' . $hours . '</h3>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			    for ($i = -4; $i <= -1; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate1+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate1+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate1+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			    echo'</div><div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>';
    			    for ($i = 0; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate1+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate1+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate1+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			?>
    		</div>
    		</div>
            </div>
    		

    <!Après demain>

    		

    		<div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    		    <div class="meteo_box_byday">

        			<h2><?php echo $deuxjours. " " .$deux;?></h2>

        			

        			<img class="icon_meteo" src="images/icon_<?php echo $icon2?>" alt="icon_meteo.png"><br>

        			

        			<h3 class="tempaff">

        			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

        			<?php echo $temp_min2?> °C</h3><br>

        			

        			<h3 class="tempaff">

        			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

        			<?php echo $temp_max2?> °C</h3><br>

    			</div>
                </div>
                <div class="row">
    			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>

    			<?php

    			    for ($i = -4; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate2+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate2+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate2+$i]->dt_txt,-8,5);

                        echo '

                        <div class="hidden-xs hidden-sm col-md-1 col-lg-1 hoursforecast">

                        <h3>' . $hours . '</h3>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>

                        ';

    			    }
    			    for ($i = -4; $i <= -1; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate2+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate2+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate2+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			    echo'</div><div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>';
    			    for ($i = 0; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate2+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate2+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate2+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }

    			?>

    			

            </div>
    		</div>

    		

    <!Dans 3 jours>

    		

    		<div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    		    <div class="meteo_box_byday">  

    			<h2><?php echo $troisjours. " " .$trois;?></h2>

    			

    			<img class="icon_meteo" src="images/icon_<?php echo $icon3?>" alt="icon_meteo.png"><br>

    			

    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $temp_min3?> °C</h3><br>

    			

    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $temp_max3?> °C</h3><br>

    			</div>
                </div>
                <div class="row">
    			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>

    			<?php

    			    for ($i = -4; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate3+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate3+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate3+$i]->dt_txt,-8,5);
                        
                        echo '

                        <div class="hidden-xs hidden-sm col-md-1 col-lg-1 hoursforecast">

                        <h3>' . $hours . '</h3>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>

                        ';

    			    }
                    for ($i = -4; $i <= -1; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate3+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate3+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate3+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			    echo'</div><div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>';
    			    for ($i = 0; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate3+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate3+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate3+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			?>
    		</div>
    		</div>

    <!Dans 4 jours>

    		

    		<div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    		    <div class="meteo_box_byday">

    			<h2><?php echo $quatrejours. " " .$quatre;?></h2>

    			

    			<img class="icon_meteo" src="images/icon_<?php echo $icon4?>" alt="icon_meteo.png"><br>

    			

    			<h3 class="tempaff">

    			    <img class="icon_tempmin" src="images/icon_tempmin.png" alt="icon_tempmin.png">

    			<?php echo $temp_min4?> °C</h3><br>

    			

    			<h3 class="tempaff">

    			    <img class="icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    			<?php echo $temp_max4?> °C</h3><br>

    			</div>
                </div>
                <div class="row">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    			

    			<?php

    			    for ($i = -4; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate4+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate4+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate4+$i]->dt_txt,-8,5);

                        echo '

                        <div class="hidden-xs hidden-sm col-md-1 col-lg-1 hoursforecast">

                        <h3>' . $hours . '</h3>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
    			        
                        
                        ';
    			    }
                    for ($i = -4; $i <= -1; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate4+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate4+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate4+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			    echo'</div><div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>';
    			    for ($i = 0; $i <= 3; $i++) {

    			        $tempmaxof3hours = $forecast_data->list[$numerolistdate4+$i]->main->temp_max;

    			        $geticonof3hours = str_replace("n", "d", $forecast_data->list[$numerolistdate4+$i]->weather[0]->icon);

                        $iconof3hours= $geticonof3hours . ".png";

                        $hours = substr($forecast_data->list[$numerolistdate4+$i]->dt_txt,-8,5);

                        echo '

                        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg hoursforecast">

                        <h4>' . $hours . '</h4>

    			            

    			            <h3 class="mini_tempaff">

    			                <img class="mini_icon_tempmax" src="images/icon_tempmax.png" alt="icon_tempmax.png">

    		            	' . $tempmaxof3hours . ' °C</h3><br>

    		            	

    		            	<img class="mini_icon_meteo" src="images/icon_' . $iconof3hours . '" alt="icon_meteo.png">

    			        </div>
                        ';
    			    }
    			?>

            </div>
    		</div>

    	</center>

    </div>

</section>



