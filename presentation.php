<div class="col-md-10 col-md-offset-1 container_pres">
	<div class="row">
		<div class="col-md-4">
			<div class="col-md-10 col-md-offset-1">
				<center>
					<img src="images/icon_poids.png" alt="icon_poids.png" class="icon_pres">
					<h2>Mesure de la masse</h2>
					<p>Vous pouvez regarder la masse en temps réel de la ruche et voir son évolution sur un graphique.</p>
				</center>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-10 col-md-offset-1">
				<center>
					<img src="images/icon_temp.png" alt="icon_temp.png" class="icon_pres">
					<h2>Mesure de la température</h2>
					<p>Vous pouvez regarder la température de la ruche en temps réel et voir son évolution sur un graphique.</p>
				</center>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-10 col-md-offset-1">
				<center>
					<img src="images/icon_bee.png" alt="icon_bee.png" class="icon_pres">
					<h2>Présence d'abeille dans la ruche</h2>
					<p>Grâce à notre système nous pouvons savoir si il y a ou non des abeille dans la ruche.</p>
				</center>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5 col-md-offset-1">
			<div class="col-md-10 col-md-offset-1">
				<center>
					<img src="images/icon_person.png" alt="icon_person.png" class="icon_pres">
					<h2>Détection d'une personne</h2>
					<p>Nous devons informer toutes personnes s'approchant de la ruche qu'il y a un danger.</p>
				</center>
			</div>
		</div>
		<div class="col-md-5">
			<div class="col-md-10 col-md-offset-1">
				<center>
					<img src="images/icon_budget.png" alt="icon_budget.png" class="icon_pres">
					<h2>Limite de budget</h2>
					<p>Nous avons comme limite de budget 100 € pour tout le matériel.</p>
				</center>
			</div>
		</div>
	</div>
</div>