<?php
	include_once 'header.php';
?>

<?php
        include_once 'includes/dbconnection.php';
    $result = mysqli_query($conn,"SELECT id,temperature,poids,DATE_FORMAT(time,'%d/%m %H:%i:%s') FROM ruche_info ORDER BY id DESC LIMIT 120;");
    while ($row = mysqli_fetch_array($result)) 
    {
        $donnees[$row['id']]['id'] = $row['id'];
        $donnees[$row['id']]['temperature'] = $row['temperature'];
        $donnees[$row['id']]['poids'] = $row['poids'];
        $donnees[$row['id']]['time'] = $row['3'];
    }
    /*foreach($donnees as $array){
        echo $array['id'].'<br />';
        echo $array['temperature'].'<br />';
        echo $array['poids'].'<br />';
        echo $array['time'].'<br />';
    }*/

?>
<div  class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
    <canvas id="graphique_temp"></canvas>
</div>


<script>
        var config = {
            type: 'line',
            data: {
                labels: [
                <?php
                foreach(array_reverse($donnees) as $newdonnees){
                        echo '"'.$newdonnees['time'].'",';
                }?>
                ],
                datasets: [{
                    label: "Température",
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                    <?php
                    foreach(array_reverse($donnees) as $newdonnees){
                        echo '"'.$newdonnees['temperature'].'",';
                    }?>
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:'Temperature en fonction du temps'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                    enabled: true,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Température (°C)'
                        }
                    }]
                },
                trendlineLinear: {
                    style: "rgba(42,63,84, .2)",
                    width: 2
                },
                legend:{
                    display: false,
                    position: 'bottom',
                    fullWidth: true,
                    reverse: false,
                }
            }
        };
        
        var ctx = document.getElementById("graphique_temp").getContext("2d");
        window.myLine = new Chart(ctx, config).Line(lineChartData, {
            responsive: true,
            showTooltips: true,
     });
       
       
 
       
    </script>
    
    
<?php
	include_once 'footer.php';
?>
