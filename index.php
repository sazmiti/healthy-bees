<?php
	include_once("header.php");
?>

<body>

<?php
	include_once("weather.php");
?>

<?php
	if (isset($_SESSION['id'])) {
		include_once("value.php");
		include_once("error_table.php");
	}
	else {
		include_once("presentation.php");
	}
?>

</body>

<?php
	include_once("footer.php");
?>