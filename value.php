<?php
        include_once 'includes/dbconnection.php';
    $result = mysqli_query($conn,"SELECT id,temperature,poids,son,DATE_FORMAT(time,'%d/%m %H:%i:%s') FROM ruche_info ORDER BY id DESC LIMIT 2;");

    $value = mysqli_fetch_array($result);
    
    $id = $value['id'];
    $temp = $value['temperature'];
    $poids = $value['poids'];
    $bees = $value['son'];
    $time = $value['4'];

    $value = mysqli_fetch_array($result);

    $idback = $value['id'];
    $tempback = $value['temperature'];
    $poidsback = $value['poids'];
    $beesback = $value['son'];
    $timeback = $value['4'];

    $Wmoins = $poidsback - 2;
    $Wplus = $poidsback + 2;

    $Tmoins = $tempback - 2;
    $Tplus = $tempback + 2;
?>


<div id="refresh">
 <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-md-offset-1 col-lg-offset-1 box_info_ruche">
        <center>
            <img src="images/icon_poids.png" alt="icon_poids.png" class="icon_value">
            <h2>Poids de la ruche</h2>
            <p class="text_value" style="<?php if ($poids > $Wplus || $poids < $Wmoins) {
                echo "color: red";
            }?>">
            <?php
                echo $poids . " Kg";
            ?></p>
        </center>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 box_info_ruche">
        <center>
            <img src="images/icon_temp.png" alt="icon_temp.png" class="icon_value">
            <h2>Température interne</h2>
            <p class="text_value" style="<?php if ($temp > $Tplus || $temp < $Tmoins) {
                echo "color: red";
            }?>">
            <?php
                echo $temp . "°C";
            ?></p>
        </center>
    </div>
    
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5  col-md-offset-1 col-lg-offset-1 box_info_ruche">
        <center>
            <img src="images/icon_bee.png" alt="icon_bee.png" class="icon_value">
            <h2>Présence d'abeille</h2>
            <p class="text_value"><?php
                if ($bees != 0) {
                    echo "Il y a des abeilles dans la ruche.";
                }else{
                    echo "Il n'y a plus d'abeilles.";
                }
            ?></p>
        </center>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 box_info_ruche">
        <center>
            <img src="images/icon_clock.png" alt="icon_clock.png" class="icon_value">
            <h2>Dernière vérification des valeurs</h2>
            <p class="text_value"><?php
                echo $time;
            ?></p>
        </center>
    </div>
 </div>
</div>

<script type="text/javascript">
var auto_refresh = setInterval(
function ()
{
$('#refresh').load('value.php').fadeIn("slow");
}, 30000); // refresh every 10000 milliseconds
</script>
