<?php

if (isset($_POST['submit'])) {
	
	include_once 'dbconnection.php';

	$name = htmlspecialchars($_POST['nom']);
	$surname = htmlspecialchars($_POST['prenom']);
	$birthday = htmlspecialchars($_POST['anniv']);
	$email = htmlspecialchars($_POST['email']);
	$pwd = htmlspecialchars($_POST['pwd']);


//ERROR CHECKER
	//CHECK FOR EMPTY

	if (empty($name) || empty($surname) || empty($birthday) || empty($email) || empty($pwd)) {
		header("Location: ../inscription.php?signup=empty");
		exit();
	}
	else{
		//CHECK FOR INVALID FORMAT

		if (!preg_match("/^[a-zA-Z]*$/", $name) || !preg_match("/^[a-zA-Z]*$/", $surname)) {
			header("Location: ../inscription.php?signup=invalid");
			exit();
		}
		else{
			//CHECK IF THE EMAIL IS VALID

			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				header("Location: ../inscription.php?signup=email");
				exit();
			}
			else{
				//CHECK IF THE EMAIL IS TAKEN

				$sql = "SELECT * FROM users WHERE email = '$email'";
				$result = mysqli_query($conn, $sql);
				$resultcheck = mysqli_num_rows($result);

				if ($resultcheck > 0) {
					header("Location: ../inscription.php?signup=emailtaken");
					exit();
				}
				else{
					//ALL THE FIELD ARE RIGHT AND VERIFIED SO CAN ENTER TO THE DATABASE
					//ENCODE PASSWORD

					$hashedpwd = password_hash($pwd, PASSWORD_DEFAULT);

					//GO TO THE DATABASE

					$sql = "INSERT INTO users (nom, prenom, email, pwd, anniv) VALUES ('$name', '$surname', '$email', '$hashedpwd', '$birthday');";
					mysqli_query($conn, $sql);


					//EXIT THE FILE

					header("Location: ../inscription.php?signup=success");
					exit();

				}


			}


		}

	}



}
else{
	header("Location: ../inscription.php");
	exit();
}