<?php 

session_start();

if (isset($_POST['submit'])) {
	
	include_once 'dbconnection.php';

	$email = htmlspecialchars($_POST['email']);
	$pwd = htmlspecialchars($_POST['pwd']);

//ERROR CHECKER
	//CHECK FOR EMPTY

	if (empty($email) || empty($pwd)) {
		header("Location: ../connection.php?login=empty");
		exit();
	}
	else{
		//CHECK IF THE EMAIL EXIST

		$sql = "SELECT * FROM users WHERE email = '$email'";
		$result = mysqli_query($conn, $sql);
		$resultcheck = mysqli_num_rows($result);

		if ($resultcheck < 1) {
			header("Location: ../connection.php?login=error");
			exit();
		}
		else{

			if ($row = mysqli_fetch_assoc($result)) {
				//DECODE THE PASSWORD
				$hashedpwdcheck = password_verify($pwd, $row['pwd']);

				if ($hashedpwdcheck == false) {
					header("Location: ../connection.php?login=error");
					exit();
				}
				elseif ($hashedpwdcheck == true) {
					//CONNECTION 
					$_SESSION['id'] = $row['id'];
					$_SESSION['nom'] = $row['nom'];
					$_SESSION['prenom'] = $row['prenom'];
					$_SESSION['email'] = $row['email'];
					$_SESSION['anniv'] = $row['anniv'];

					header("Location: ../index.php");
					exit();
				}
			}

		}


	}
}
else{
	header("Location: ../connection.php?login=error");
	exit();
}
