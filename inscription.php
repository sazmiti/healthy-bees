<?php
    include_once "header.php";
    
	error_reporting(E_ALL & ~E_NOTICE);
?>

<body>


<!INSCRIPTION>



<section class="login">

	<div class="col-xs-10 col-sm-10 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-1 col-md-offset-4 col-lg-offset-4 inscription-style">

		<center>

			<h2><strong>Inscription</strong></h2>
			<form class="form_connection" action="includes/signup.php" method="POST">

			<div class="form-group">
    			<label for="inputname">Nom :</label>
    			<input type="text" class="form-control" id="inputname" placeholder="Nom" name="nom">
  			</div>
  			<div class="form-group">
    			<label for="inputsurname">Prénom :</label>
    			<input type="text" class="form-control" id="inputsurname" placeholder="Prénom" name="prenom">
  			</div>
  			<div class="form-group">
    			<label for="inputanniv">Date de naissance :</label>
    			<input type="date" class="form-control" id="inputanniv" placeholder="jj/mm/aaaa" name="anniv">
  			</div>
  			<div class="form-group">
    			<label for="inputemail">Adresse Email :</label>
    			<input type="email" class="form-control" id="inputemail" placeholder="Email" name="email">
  			</div>
  			<div class="form-group">
    			<label for="inputnamepwd">Mot de passe :</label>
    			<input type="password" class="form-control" id="inputpwd" placeholder="Password" name="pwd">
  			</div>

			<button type="submit" name="submit" class="btn btn-primary">S'inscrire</button>

			<?php

			    $signup = $_GET['signup'];

			    if(isset($signup)){

			        if($signup == 'empty'){

			            echo '<h3 class="error_red">Vous n\'avez pas rempli tous les champs !</h3>';

			        }

			        if($signup == 'invalid'){

			            echo '<h3 class="error_red">Merci de rentrer des Nom et Prénom constituées de lettres uniquement.</h3>';

			        }

			        if($signup == 'email'){

			            echo '<h3 class="error_red">Merci de mettre un e-mail valide.</h3>';

			        }

			        if($signup == 'emailtaken'){

			            echo '<h3 class="error_red">Votre adresse email est déjà prise.</h3>';

			        }

			        if($signup == 'success'){

			            echo '<h3 class="success_green">Votre inscription a été prise en compte.</h3>';

			        }

			    }

			?>

			</form>

			<a href="connection.php">Déjà inscrit ?</a>

		</div>

	</center>

</section>


</body>


<?php

	include_once 'footer.php';

?>