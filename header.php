<?php
  session_start();
?>


<!DOCTYPE html>
<html>

<head>
  <title>HEALTHY BEES</title>
  <meta charset="utf-8-general-ci">
  <link rel="icon" href="images/icon_bee.png" />
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/asset/utils.js"></script>
  <script src="js/asset/trendline.js"></script>
  <script src="js/asset/chart.js"></script>
</head>



<nav class="navbar navbar-inverse nav-style">
  <div class="col-md-10 col-md-offset-1">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php"><img src="images/icon.png" alt="icon.png" class="icon_acceuil"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Accueil</a></li>
        <li><a href="meteo.php"><span class="glyphicon glyphicon-cloud"></span> Météo</a></li>
        <?php
                if (isset($_SESSION['id'])) {
                  echo '
                 	<li class="dropdown">
         			 	<a href="graphique.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-stats"></span> Graphiques<span class="caret"></span></a>
          				<ul class="dropdown-menu">
            				<li><a href="graphique_force.php">Force</a></li>
            				<li class="divider"></li>
            				<li><a href="graphique_temperature.php">Température</a></li>
          				</ul>
        			</li>
                  	<li>
                  	<form class="logout" action="includes/logout.php" method="POST">
                  	<button type="submit" name="submit" class="btn btn-warning logout_btn"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</button>
                  	</form>
                  	</li>';
                }
                else{
                  echo '
                  	<li><a href="connection.php">Connexion</a></li>
                  	<li><a href="inscription.php">Inscription</a></li>';
                }
              ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
