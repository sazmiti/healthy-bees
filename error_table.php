<?php
	include_once 'includes/dbconnection.php';
    $result = mysqli_query($conn,"SELECT id,probleme,DATE_FORMAT(date,'%d/%m %H:%i:%s') FROM error_info ORDER BY id DESC LIMIT 50;");

    while ($row = mysqli_fetch_array($result)) 
    {
    	$donnees[$row['id']]['id'] = $row['id'];
        $donnees[$row['id']]['probleme'] = $row['probleme'];
        $donnees[$row['id']]['date'] = $row['2'];
    }

?>


<div id="refresherror">
	<div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 box_info_error">
		<center><h2>Gestionaire des problèmes</h2></center>
		<div class="table_div">
			<table class="table table-condensed">
				<thead> 
					<tr> 
						<th>Date du problème</th> 
						<th>Facteur mis en cause</th>  
					</tr> 
				</thead> 
				<tbody> 
					<?php
						foreach ($donnees as $value) {
							switch ($value["probleme"]) {
								case 'weightmax':
									$typeoferror = "Le poids maximum de la ruche à été dépasser.";
									break;
								case 'weightmin':
									$typeoferror = "Il doit y avoir une erreur avec les capteur de force.";
									break;
								case 'weightbrusque':
									$typeoferror = "Il y a eu un changement brusque de poids.";
									break;
								case 'tempmax':
									$typeoferror = "La température maximale de la ruche à été dépasser.";
									break;
								case 'tempmin':
									$typeoferror = "La température de la ruche est inférieure à 8°C.";
									break;
								case 'tempbrusque':
									$typeoferror = "Il y a eu un changement brusque de température.";
									break;
								default:
									$typeoferror = "Il y a une erreur indéfinie avec la ruche.";
									break;
							}
							echo "<tr>";
							echo "<th scope=row>".$value["date"]."</th>";
							echo "<td>".$typeoferror."</td>";
							echo "</tr>";
						}
					?>
				</tbody> 
			</table>
		</div>
	</div>
</div>
	
<script type="text/javascript">
var auto_refresh = setInterval(
function ()
{
$('#refresherror').load('error_table.php').fadeIn("slow");
}, 30000); // refresh every 10000 milliseconds
</script>
